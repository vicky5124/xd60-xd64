To install the .hex file inside the keyboard you need to:

- Set the keyboard on reset
- Run this commands in order:
```
dfu-programmer atmega32u4 erase --force
dfu-programmer atmega32u4 flash xd60_rev2_layout_all_mine.hex
dfu-programmer atmega32u4 reset
```
And there youll have the layout. (commands may require root acccess)